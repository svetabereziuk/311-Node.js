module.exports = class MyNumber {
  constructor(value) {
    this.value = value;
  }

  getDigitsNumber() {
    let number = this.value;

    let count = 0;
    if (number > 0) {
      count += 1;
    }

    while (number / 10 >= 1) {
      count += 1;
      number /= 10;
    }
    return count;
  }

  getDigitsSum() {
    let number = this.value;
    let sum = 0;

    while (number > 0) {
      sum += number % 10;
      number = Math.floor(number / 10);
    }

    return sum;
  }

  isPerfect(number) {
    this.number = number;
    let temp = 0;
    for (let i = 1; i <= this.number / 2; i += 1) {
      if (this.number % i === 0) {
        temp += i;
      }
    }

    if (temp === this.number && temp !== 0) {
      return true;
    }

    return false;
  }

  countOfSmallerPerfectNumbers() {
    const number = this.value;
    let count = 0;
    for (let i = 1; i < number; i += 1) {
      if (this.isPerfect(i)) {
        count += 1;
      }
    }
    return count;
  }
};

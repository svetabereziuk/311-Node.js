var http = require('http');
var static = require('node-static');
var file = new static.Server('.');
let url = require('url');

http.createServer(function(req, res) {
		
		let path = url.parse(req.url).pathname;
		let text;
    if(path=='/'){
		  text='Hello world!';
	  }
	  else if (path=='/hi'){
		  text='Hi!';
	  }
	  else{
		  text='Page not found';
	  }
		res.write(text);
    res.end();

  
}).listen(8080);

console.log('Server running on port 8080');
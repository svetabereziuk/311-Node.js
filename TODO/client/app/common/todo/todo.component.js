import template from './todo.html';
import controller from './todo.controller';
import './todo.scss';

let TodoComponent = {
  bindings: {
  },
  template,
  controller
};

export default TodoComponent;

class TodoFilterController {
    $onChanges(changes) {
      if (changes.filterText) {
        this.filterText = Object.assign({}, this.TodoFilterController);
      }
    }
    onChange() {
      if (!this.filter.text) return;
        this.onChangeFilter({
          $event: {
            filterText: this.filter.text
          }
        });
	  }
}

export default TodoFilterController;

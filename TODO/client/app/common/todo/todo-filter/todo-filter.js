import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoFilterComponent from './todo-filter.component';

let todoFilterModule = angular.module('todoFilter', [
  uiRouter
])

.component('todoFilter', todoFilterComponent)
  
.name;

export default todoFilterModule;

import template from './todo-filter.html';
import controller from './todo-filter.controller';

let todoFilterComponent = {
  bindings: {
    filter: '<',
    onChangeFilter: '&'
  },
  template,
  controller
};

export default todoFilterComponent;

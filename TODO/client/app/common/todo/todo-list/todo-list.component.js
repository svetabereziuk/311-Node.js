import template from './todo-list.html';
import controller from './todo-list.controller';
import './todo-list.scss';

let todoListComponent = {
  bindings: {
    todos: '<',
    filter: '<',
    onRemoveTodo: '&'
  },
  template,
  controller
};

export default todoListComponent;

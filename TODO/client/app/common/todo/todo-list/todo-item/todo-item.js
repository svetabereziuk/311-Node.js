import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoItemComponent from './todo-item.component';

let todoItemModule = angular.module('todoItem', [
  uiRouter
])

.component('todoItem', todoItemComponent)
  
.name;

export default todoItemModule;

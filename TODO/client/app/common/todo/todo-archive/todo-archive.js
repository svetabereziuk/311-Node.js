import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoArchiveComponent from './todo-archive.component';

let todoArchiveModule = angular.module('todoArchive', [
  uiRouter
])

.component('todoArchive', todoArchiveComponent)
  
.name;

export default todoArchiveModule;

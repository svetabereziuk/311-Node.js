import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoRemainComponent from './todo-remain.component';

let todoRemainModule = angular.module('todoRemain', [
  uiRouter
])

.component('todoRemain', todoRemainComponent)
  
.name;

export default todoRemainModule;

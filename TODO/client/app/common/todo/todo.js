import angular from 'angular';
import TodoService from './todo.service';
import uiRouter from 'angular-ui-router';
import todoComponent from './todo.component';

let todoModule = angular.module('todo', [
  uiRouter
])
.service('TodoService',TodoService)

.component('todo', todoComponent)

.name;

export default todoModule;

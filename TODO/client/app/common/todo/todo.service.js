class TodoService {
  getTodos() {
    let promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve([
          {text:'learn angular', done:true},
          {text:'build an angular app', done:false},
          {text:'modify', done:true},
          {text:'test', done:false},
          {text:'close', done:false}
        ]);
      }, 1);    
    });
    return promise;
  }
}
export default TodoService;
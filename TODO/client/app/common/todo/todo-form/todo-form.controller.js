class TodoFormController {
    $onChanges(changes) {
      if (changes.todo) {
        this.todo = Object.assign({}, this.todo);
      }
      if (changes.filterText) {
        this.filterText = Object.assign({}, this.filterText);
      }
    }
    onSubmit() {
      if (!this.todo.text) return;
      this.onAddTodo({
        $event: {
          todo: this.todo
        }
    });
	}
}

export default TodoFormController;

class TodoController {
	constructor(TodoService) {
    'ngInject';
    this.todoService = TodoService;

  }
  $onInit() {
	  this.newTodo = {
		text: '',
		done: false
		};

	  this.filter = {
			text: ''
		};

		this.todos = [];
		this.todoService.getTodos().then(response => this.todos = response);
		
  }
	addTodo({ todo }) {
	  if (!todo) return;
	  this.todos.unshift(todo);
	  this.newTodo = {
			text: '',
			done: false
	  };
	}

	removeTodo({ index }) {
	  this.todos.splice(index,1);
	}	

	changeFilter({ filterText }) {
	  if (!filterText) return;
	  this.filter.text = filterText;
	}

	addToArchive() {
	  this.todos = this.todos.filter(todo => !todo.done);
	}

	getRemaining() {
	  let remain = this.todos.filter(todo => !todo.done);
    return remain.length;
	}
}

export default TodoController;

import angular from 'angular';
import Navbar from './navbar/navbar';
import Hero from './hero/hero';
import User from './user/user';
import TodoList from './todo/todo-list/todo-list';
import TodoForm from './todo/todo-form/todo-form';
import TodoFilter from './todo/todo-filter/todo-filter';
import TodoArchive from './todo/todo-archive/todo-archive';
import TodoRemain from './todo/todo-remain/todo-remain';
import TodoItem from './todo/todo-list/todo-item/todo-item';
import Todo from './todo/todo';

let commonModule = angular.module('app.common', [
  Navbar,
  Todo,
  TodoFilter,
  TodoForm,
  TodoList,
  TodoItem,
  TodoArchive,
  TodoRemain,
  Hero,
  User
])

.name;

export default commonModule;

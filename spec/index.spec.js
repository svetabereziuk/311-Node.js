const MyNumber=require("..task1//my-number.js");
describe('Number operations', function () {
  it('Number of digits 34567', function () {
    expect(new MyNumber(34567).getDigitsNumber()).toBe(5);
  });
  it('Number of digits 0', function () {
    expect(new MyNumber(0).getDigitsNumber()).toBe(0);
  });
  it('Count of perfect numbers smaller then number 497', function () {
    expect(new MyNumber(497).countOfSmallerPerfectNumbers()).toBe(3);
  });
  it('Count of perfect numbers smaller then number 496', function () {
    expect(new MyNumber(496).countOfSmallerPerfectNumbers()).toBe(2);
  });
});

